package pl.wat.mgr.witowski.ms.mgsauth.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.wat.mgr.witowski.ms.mgsauth.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.ms.mgsauth.dto.LoginDto;
import pl.wat.mgr.witowski.ms.mgsauth.dto.TokenDto;

@Log4j2
@Component
@Profile("nonAuth")
public class KeyServiceUtils {
    private static String microKey;

    private static String mgrAuthUrl;
    private static String mgrAuthLogin;
    private static String mgrAuthPassword;

    @Value("${pl.wat.mgr.witowski.ms.mgrauth.url.mgrauth}")
    public void setmgrAuthUrl(String mgrAuthUrl) {
        KeyServiceUtils.mgrAuthUrl = mgrAuthUrl + "/login";
    }

    @Value("${pl.mgr.ms.auth.login}")
    public void setmgrAuthLogin(String mgrAuthLogin) {
        KeyServiceUtils.mgrAuthLogin = mgrAuthLogin;
    }

    @Value("${pl.mgr.ms.auth.password}")
    public void setmgrAuthPassword(String mgrAuthPassword) {
        KeyServiceUtils.mgrAuthPassword = mgrAuthPassword;
    }

    public static String refrestMicroToken() {
        KeyServiceUtils.microKey = null;
        return getMicroToken();
    }

    public static String getMicroToken() {
        if (!StringUtils.hasText(KeyServiceUtils.microKey) || !JwtUtils.validateToken(KeyServiceUtils.microKey)) {
            RestTemplate restTemplate = new RestTemplate();
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(KeyServiceUtils.mgrAuthUrl)
                    // Add query parameter
                    .queryParam("type", "INNER_USER");
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(builder.build().toUri(), LoginDto.builder()
                    .login(KeyServiceUtils.mgrAuthLogin)
                    .password(KeyServiceUtils.mgrAuthPassword)
                    .build(), String.class);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                if (responseEntity.getStatusCode() == HttpStatus.NO_CONTENT) {
                    throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "auth-all-001", "auth-all-001");
                }
                if (responseEntity.getStatusCode() == HttpStatus.OK) {
                    KeyServiceUtils.microKey = convertJsonToTokenDto(responseEntity.getBody()).getToken();
                }
            } else if (responseEntity.getStatusCode().is5xxServerError()) {
                ApiExceptions apiExceptions = convertJsonToApiExeption(responseEntity.getBody());
                throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "auth-all-002", "auth-all-002", apiExceptions.getCode());
            }
        }
        return KeyServiceUtils.microKey;
    }

    private static ApiExceptions convertJsonToApiExeption(String body) {
        try {
            return new ObjectMapper().readValue(body, ApiExceptions.class);
        } catch (Exception ex) {
            log.error("Error occured convert json: {}", body);
            return new ApiExceptions();
        }
    }

    private static TokenDto convertJsonToTokenDto(String body) {
        try {
            return new ObjectMapper().readValue(body, TokenDto.class);
        } catch (Exception ex) {
            log.error("Error occured convert json: {}", body);
            return new TokenDto();
        }
    }

    public static String getMicroUid() {
        return JwtUtils.getUUID(KeyServiceUtils.getMicroToken());
    }
}
