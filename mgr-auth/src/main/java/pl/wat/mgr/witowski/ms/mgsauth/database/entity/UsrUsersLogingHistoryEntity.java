package pl.wat.mgr.witowski.ms.mgsauth.database.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "usr_users_loging_history")
public class UsrUsersLogingHistoryEntity {
    @Id
    @Column(name = "usr_log_his_uid")
    private String usrLogHisUid;
    @Basic
    @Column(name = "usr_log_his_act")
    private short usrLogHisAct;
    @Basic
    @Column(name = "usr_log_his_data")
    private LocalDateTime usrLogHisData;
    @Basic
    @Column(name = "usr_log_his_ip")
    private String usrLogHisIp;
    @Basic
    @Column(name = "usr_log_his_location")
    private String usrLogHisLocation;
    @Basic
    @Column(name = "usr_log_his_status")
    private String usrLogHisStatus;
    @ManyToOne
    @JoinColumn(name = "usr_usr_id", referencedColumnName = "usr_id", nullable = false)
    private UsrUsersEntity usrUsersByUsrUsrId;

    public String getUsrLogHisUid() {
        return usrLogHisUid;
    }

    public void setUsrLogHisUid(String usrLogHisUid) {
        this.usrLogHisUid = usrLogHisUid;
    }

    public short getUsrLogHisAct() {
        return usrLogHisAct;
    }

    public void setUsrLogHisAct(short usrLogHisAct) {
        this.usrLogHisAct = usrLogHisAct;
    }

    public LocalDateTime getUsrLogHisData() {
        return usrLogHisData;
    }

    public void setUsrLogHisData(LocalDateTime usrLogHisData) {
        this.usrLogHisData = usrLogHisData;
    }

    public String getUsrLogHisIp() {
        return usrLogHisIp;
    }

    public void setUsrLogHisIp(String usrLogHisIp) {
        this.usrLogHisIp = usrLogHisIp;
    }

    public String getUsrLogHisLocation() {
        return usrLogHisLocation;
    }

    public void setUsrLogHisLocation(String usrLogHisLocation) {
        this.usrLogHisLocation = usrLogHisLocation;
    }

    public String getUsrLogHisStatus() {
        return usrLogHisStatus;
    }

    public void setUsrLogHisStatus(String usrLogHisStatus) {
        this.usrLogHisStatus = usrLogHisStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsrUsersLogingHistoryEntity that = (UsrUsersLogingHistoryEntity) o;

        if (usrLogHisAct != that.usrLogHisAct) return false;
        if (usrLogHisUid != null ? !usrLogHisUid.equals(that.usrLogHisUid) : that.usrLogHisUid != null) return false;
        if (usrLogHisData != null ? !usrLogHisData.equals(that.usrLogHisData) : that.usrLogHisData != null)
            return false;
        if (usrLogHisIp != null ? !usrLogHisIp.equals(that.usrLogHisIp) : that.usrLogHisIp != null) return false;
        if (usrLogHisLocation != null ? !usrLogHisLocation.equals(that.usrLogHisLocation) : that.usrLogHisLocation != null)
            return false;
        if (usrLogHisStatus != null ? !usrLogHisStatus.equals(that.usrLogHisStatus) : that.usrLogHisStatus != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = usrLogHisUid != null ? usrLogHisUid.hashCode() : 0;
        result = 31 * result + (int) usrLogHisAct;
        result = 31 * result + (usrLogHisData != null ? usrLogHisData.hashCode() : 0);
        result = 31 * result + (usrLogHisIp != null ? usrLogHisIp.hashCode() : 0);
        result = 31 * result + (usrLogHisLocation != null ? usrLogHisLocation.hashCode() : 0);
        result = 31 * result + (usrLogHisStatus != null ? usrLogHisStatus.hashCode() : 0);
        return result;
    }

    public UsrUsersEntity getUsrUsersByUsrUsrId() {
        return usrUsersByUsrUsrId;
    }

    public void setUsrUsersByUsrUsrId(UsrUsersEntity usrUsersByUsrUsrId) {
        this.usrUsersByUsrUsrId = usrUsersByUsrUsrId;
    }
}
