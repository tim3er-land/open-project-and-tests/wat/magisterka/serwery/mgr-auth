package pl.wat.mgr.witowski.ms.mgsauth.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import pl.wat.mgr.witowski.ms.mgsauth.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.ms.mgsauth.database.entity.UsrRolesEntity;
import pl.wat.mgr.witowski.ms.mgsauth.database.entity.UsrUsersEntity;
import pl.wat.mgr.witowski.ms.mgsauth.database.entity.UsrUsersLogingHistoryEntity;
import pl.wat.mgr.witowski.ms.mgsauth.database.repository.UsrRoleRepository;
import pl.wat.mgr.witowski.ms.mgsauth.database.repository.UsrUsersLoginHistoryRepository;
import pl.wat.mgr.witowski.ms.mgsauth.database.repository.UsrUsersRepository;
import pl.wat.mgr.witowski.ms.mgsauth.dto.LoginDto;
import pl.wat.mgr.witowski.ms.mgsauth.dto.TokenDto;
import pl.wat.mgr.witowski.ms.mgsauth.security.TokenGeneratorService;
import pl.wat.mgr.witowski.ms.mgsauth.security.jwt.SecurityUtils;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@Service
public class LoginService {

    private final UsrUsersRepository usrUsersRepository;
    private final TokenGeneratorService tokenGeneratorService;
    private final UsrUsersLoginHistoryRepository usrUsersLoginHistoryRepository;
    private final UsrRoleRepository usrRoleRepository;
    private final Integer loginErrorCounter;

    @Autowired
    public LoginService(UsrUsersRepository usrUsersRepository
            , TokenGeneratorService tokenGeneratorService
            , UsrUsersLoginHistoryRepository usrUsersLoginHistoryRepository, UsrRoleRepository usrRoleRepository, @Value("${pl.wat.mgr.witowski.ms.mgrauth.autorization.login.error}") Integer loginErrorCounter) {
        this.usrUsersRepository = usrUsersRepository;
        this.tokenGeneratorService = tokenGeneratorService;
        this.usrUsersLoginHistoryRepository = usrUsersLoginHistoryRepository;
        this.usrRoleRepository = usrRoleRepository;
        this.loginErrorCounter = loginErrorCounter;
    }


    public TokenDto login(LoginDto loginDto, HttpServletRequest request, String userType) {
        if (StringUtils.isEmpty(loginDto.getLogin()) || StringUtils.isEmpty(loginDto.getPassword())) {
            log.warn("Login or password is empty");
            throw new ApiExceptions(HttpStatus.BAD_REQUEST, "auth-400-001-001", "auth-400-001-001");
        }
        UsrUsersEntity usrUsers = usrUsersRepository.findUserForLogin(loginDto.getLogin().toUpperCase(), userType);
        if (usrUsers == null) {
            throw new ApiExceptions(HttpStatus.UNAUTHORIZED, "auth-401-001-002", "auth-401-001-002");
        }
        if (!usrUsers.getUsrPass().equalsIgnoreCase(SecurityUtils.hashString(loginDto.getPassword()))) {
            Integer usrErrorLogin = usrUsers.getUsrErrorLogin();
            if (usrErrorLogin < loginErrorCounter) {
                usrUsers.setUsrStatus((short) 2);
                usrUsersRepository.save(usrUsers);
                saveLoginHostory(request, "INCORECT PASSWORD", usrUsers);
                throw new ApiExceptions(HttpStatus.UNAUTHORIZED, "auth-401-001-003", "auth-401-001-003");
            } else {
                usrErrorLogin++;
                usrUsers.setUsrErrorLogin(usrErrorLogin);
                usrUsersRepository.save(usrUsers);
                saveLoginHostory(request, "USER BLOCK", usrUsers);
                throw new ApiExceptions(HttpStatus.UNAUTHORIZED, "auth-401-001-004", "auth-401-001-004");
            }
        }
        String tokenUid = UUID.randomUUID().toString();
        TokenDto tokenDto;
        List<UsrRolesEntity> userRoles = usrRoleRepository.getUserRoles(usrUsers);
        if (CollectionUtils.isEmpty(userRoles)) {
            log.warn("Can not find roles for user: {}", usrUsers.getUsrUid());
            throw new ApiExceptions(HttpStatus.UNAUTHORIZED, "auth-401-001-005", "auth-401-001-005");
        }
        List<String> roles = userRoles.stream().map(UsrRolesEntity::getRlCode).collect(Collectors.toList());
        try {
            tokenDto = tokenGeneratorService.generateJwtToken(usrUsers, tokenUid, roles);
            log.info("save token with tokenUid", tokenUid);
        } catch (Exception ex) {
            log.error("Error occured save token in redis or create token", ex);
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "auth-500-001-006", "auth-500-001-006");
        }
        saveLoginHostory(request, "SUCCESFUL", usrUsers);
        return tokenDto;
    }

    private void saveLoginHostory(HttpServletRequest request, String status, UsrUsersEntity usrUsers) {
        UsrUsersLogingHistoryEntity logingHistory = new UsrUsersLogingHistoryEntity();
        logingHistory.setUsrLogHisIp(request.getRemoteAddr());
        logingHistory.setUsrLogHisAct((short) 1);
        logingHistory.setUsrLogHisLocation("");
        logingHistory.setUsrUsersByUsrUsrId(usrUsers);
        logingHistory.setUsrLogHisUid(UUID.randomUUID().toString());
        logingHistory.setUsrLogHisStatus(status);
        logingHistory.setUsrLogHisData(LocalDateTime.now());
        usrUsersLoginHistoryRepository.save(logingHistory);
    }
}
