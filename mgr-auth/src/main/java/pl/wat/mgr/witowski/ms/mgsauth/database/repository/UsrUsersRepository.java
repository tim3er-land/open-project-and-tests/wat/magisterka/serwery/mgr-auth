package pl.wat.mgr.witowski.ms.mgsauth.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.wat.mgr.witowski.ms.mgsauth.database.entity.UsrUsersEntity;

@Repository
public interface UsrUsersRepository extends JpaRepository<UsrUsersEntity, Long> {

    @Query("select u from UsrUsersEntity u where u.usrAct = 1 and upper(u.usrLogin) = upper(:login) and u.usrStatus = 1 and upper(u.usrUserTypeCode) = upper(:userType) ")
    UsrUsersEntity findUserForLogin(@Param("login") String login, @Param("userType") String userType);

    UsrUsersEntity findByUsrUid(String usrUid);
}
