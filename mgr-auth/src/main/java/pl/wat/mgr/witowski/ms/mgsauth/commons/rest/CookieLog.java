package pl.wat.mgr.witowski.ms.mgsauth.commons.rest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CookieLog {
    private String name;
    private String value;
}
