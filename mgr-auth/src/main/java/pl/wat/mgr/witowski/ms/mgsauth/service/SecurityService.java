package pl.wat.mgr.witowski.ms.mgsauth.service;

import lombok.extern.log4j.Log4j2;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import pl.wat.mgr.witowski.ms.mgsauth.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.ms.mgsauth.dto.FullKeyDto;
import pl.wat.mgr.witowski.ms.mgsauth.security.JwtSecretGenerator;
import pl.wat.mgr.witowski.ms.mgsauth.security.secret.SecretDto;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Security;
import java.security.spec.RSAKeyGenParameterSpec;
import java.util.Base64;

@Service
@Log4j2
public class SecurityService {

    public SecretDto generateNewKeyV2() {
        log.info("get public key");
        return SecretDto.builder().secret(JwtSecretGenerator.getPublicKey()).build();
    }

    public FullKeyDto getPairKey() {
        try {
            log.info("start generate new pair key");
            Security.addProvider(new BouncyCastleProvider());
            KeyPairGenerator keyPair = KeyPairGenerator.getInstance("RSA", "BC");
            keyPair.initialize(new RSAKeyGenParameterSpec(4086, RSAKeyGenParameterSpec.F4));
            log.debug("initialize generator");
            KeyPair keyPair1 = keyPair.generateKeyPair();
            log.debug("generate paif of key");
            FullKeyDto build = FullKeyDto.builder()
                    .pubKey(Base64.getEncoder().encodeToString(keyPair1.getPublic().getEncoded()))
                    .priKey(Base64.getEncoder().encodeToString(keyPair1.getPrivate().getEncoded()))
                    .build();
            log.info("end generate new pair key");
            return build;
        } catch (Exception ex) {
            log.error("Error occured while generate new pair key ");
            throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "auth-400-005-001", "auth-400-005-001");
        }
    }
}
