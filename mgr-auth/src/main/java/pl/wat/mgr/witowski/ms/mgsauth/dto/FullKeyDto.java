package pl.wat.mgr.witowski.ms.mgsauth.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FullKeyDto {
    private String pubKey;
    private String priKey;
}
