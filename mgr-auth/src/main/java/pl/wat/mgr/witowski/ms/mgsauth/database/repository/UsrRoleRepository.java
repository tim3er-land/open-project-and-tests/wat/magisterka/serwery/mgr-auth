package pl.wat.mgr.witowski.ms.mgsauth.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.wat.mgr.witowski.ms.mgsauth.database.entity.UsrRolesEntity;
import pl.wat.mgr.witowski.ms.mgsauth.database.entity.UsrUsersEntity;

import java.util.List;

@Repository
public interface UsrRoleRepository extends JpaRepository<UsrRolesEntity, Long> {

    @Query("select ur from UsrRolesEntity ur join ur.usrUsersRolesByRlId uur join uur.usrUsersByUsrUsrId uu where uur.usrUsersByUsrUsrId = :userId and ur.rlAct = 1 and uur.usrRlAct = 1 ")
    List<UsrRolesEntity> getUserRoles(@Param("userId") UsrUsersEntity userId);
}
