package pl.wat.mgr.witowski.ms.mgsauth.database.entity;

import javax.persistence.*;

@Entity
@Table(name = "usr_users_roles")
public class UsrUsersRolesEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "usr_rl_id")
    private long usrRlId;
    @Basic
    @Column(name = "usr_rl_act")
    private short usrRlAct;
    @ManyToOne
    @JoinColumn(name = "usr_usr_id", referencedColumnName = "usr_id", nullable = false)
    private UsrUsersEntity usrUsersByUsrUsrId;
    @ManyToOne
    @JoinColumn(name = "rl_rl_id", referencedColumnName = "rl_id", nullable = false)
    private UsrRolesEntity usrRolesByRlRlId;

    public long getUsrRlId() {
        return usrRlId;
    }

    public void setUsrRlId(long usrRlId) {
        this.usrRlId = usrRlId;
    }


    public short getUsrRlAct() {
        return usrRlAct;
    }

    public void setUsrRlAct(short usrRlAct) {
        this.usrRlAct = usrRlAct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsrUsersRolesEntity that = (UsrUsersRolesEntity) o;

        if (usrRlId != that.usrRlId) return false;
        if (usrRlAct != that.usrRlAct) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (usrRlId ^ (usrRlId >>> 32));
        result = 31 * result + (int) usrRlAct;
        return result;
    }

    public UsrUsersEntity getUsrUsersByUsrUsrId() {
        return usrUsersByUsrUsrId;
    }

    public void setUsrUsersByUsrUsrId(UsrUsersEntity usrUsersByUsrUsrId) {
        this.usrUsersByUsrUsrId = usrUsersByUsrUsrId;
    }

    public UsrRolesEntity getUsrRolesByRlRlId() {
        return usrRolesByRlRlId;
    }

    public void setUsrRolesByRlRlId(UsrRolesEntity usrRolesByRlRlId) {
        this.usrRolesByRlRlId = usrRolesByRlRlId;
    }
}
