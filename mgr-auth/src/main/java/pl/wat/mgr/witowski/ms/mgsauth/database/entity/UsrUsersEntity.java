package pl.wat.mgr.witowski.ms.mgsauth.database.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "usr_users")
public class UsrUsersEntity {
    @Id
    @Column(name = "usr_id")
    private long usrId;
    @Basic
    @Column(name = "usr_uid")
    private String usrUid;
    @Basic
    @Column(name = "usr_login")
    private String usrLogin;
    @Basic
    @Column(name = "usr_pass")
    private String usrPass;
    @Basic
    @Column(name = "usr_act")
    private short usrAct;
    @Basic
    @Column(name = "usr_status")
    private short usrStatus;
    @Basic
    @Column(name = "usr_user_type_code")
    private String usrUserTypeCode;
    @Basic
    @Column(name = "usr_insert_date")
    private LocalDateTime usrInsertDate;
    @Basic
    @Column(name = "usr_insert_by")
    private String usrInsertBy;
    @Basic
    @Column(name = "usr_modify_by")
    private String usrModifyBy;
    @Basic
    @Column(name = "usr_modify_date")
    private LocalDateTime usrModifyDate;
    @Basic
    @Column(name = "usr_block_date")
    private LocalDateTime usrBlockDate;
    @Basic
    @Column(name = "usr_error_login")
    private Integer usrErrorLogin;
    @OneToMany(mappedBy = "usrUsersByUsrUsrId")
    private Collection<UsrUsersLogingHistoryEntity> usrUsersLogingHistoriesByUsrId;
    @OneToMany(mappedBy = "usrUsersByUsrUsrId")
    private Collection<UsrUsersRolesEntity> usrUsersRolesByUsrId;

    public long getUsrId() {
        return usrId;
    }

    public void setUsrId(long usrId) {
        this.usrId = usrId;
    }

    public String getUsrUid() {
        return usrUid;
    }

    public void setUsrUid(String usrUid) {
        this.usrUid = usrUid;
    }

    public String getUsrLogin() {
        return usrLogin;
    }

    public void setUsrLogin(String usrLogin) {
        this.usrLogin = usrLogin;
    }

    public String getUsrPass() {
        return usrPass;
    }

    public void setUsrPass(String usrPass) {
        this.usrPass = usrPass;
    }

    public short getUsrAct() {
        return usrAct;
    }

    public void setUsrAct(short usrAct) {
        this.usrAct = usrAct;
    }

    public short getUsrStatus() {
        return usrStatus;
    }

    public void setUsrStatus(short usrStatus) {
        this.usrStatus = usrStatus;
    }

    public String getUsrUserTypeCode() {
        return usrUserTypeCode;
    }

    public void setUsrUserTypeCode(String usrUserTypeCode) {
        this.usrUserTypeCode = usrUserTypeCode;
    }

    public LocalDateTime getUsrInsertDate() {
        return usrInsertDate;
    }

    public void setUsrInsertDate(LocalDateTime usrInsertDate) {
        this.usrInsertDate = usrInsertDate;
    }

    public String getUsrInsertBy() {
        return usrInsertBy;
    }

    public void setUsrInsertBy(String usrInsertBy) {
        this.usrInsertBy = usrInsertBy;
    }

    public String getUsrModifyBy() {
        return usrModifyBy;
    }

    public void setUsrModifyBy(String usrModifyBy) {
        this.usrModifyBy = usrModifyBy;
    }

    public LocalDateTime getUsrModifyDate() {
        return usrModifyDate;
    }

    public void setUsrModifyDate(LocalDateTime usrModifyDate) {
        this.usrModifyDate = usrModifyDate;
    }

    public LocalDateTime getUsrBlockDate() {
        return usrBlockDate;
    }

    public void setUsrBlockDate(LocalDateTime usrBlockDate) {
        this.usrBlockDate = usrBlockDate;
    }

    public Integer getUsrErrorLogin() {
        return usrErrorLogin;
    }

    public void setUsrErrorLogin(Integer usrErrorLogin) {
        this.usrErrorLogin = usrErrorLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsrUsersEntity that = (UsrUsersEntity) o;
        return usrId == that.usrId && usrAct == that.usrAct && usrStatus == that.usrStatus && Objects.equals(usrUid, that.usrUid) && Objects.equals(usrLogin, that.usrLogin) && Objects.equals(usrPass, that.usrPass) && Objects.equals(usrUserTypeCode, that.usrUserTypeCode) && Objects.equals(usrInsertDate, that.usrInsertDate) && Objects.equals(usrInsertBy, that.usrInsertBy) && Objects.equals(usrModifyBy, that.usrModifyBy) && Objects.equals(usrModifyDate, that.usrModifyDate) && Objects.equals(usrBlockDate, that.usrBlockDate) && Objects.equals(usrErrorLogin, that.usrErrorLogin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(usrId, usrUid, usrLogin, usrPass, usrAct, usrStatus, usrUserTypeCode, usrInsertDate, usrInsertBy, usrModifyBy, usrModifyDate, usrBlockDate, usrErrorLogin);
    }

    public Collection<UsrUsersLogingHistoryEntity> getUsrUsersLogingHistoriesByUsrId() {
        return usrUsersLogingHistoriesByUsrId;
    }

    public void setUsrUsersLogingHistoriesByUsrId(Collection<UsrUsersLogingHistoryEntity> usrUsersLogingHistoriesByUsrId) {
        this.usrUsersLogingHistoriesByUsrId = usrUsersLogingHistoriesByUsrId;
    }

    public Collection<UsrUsersRolesEntity> getUsrUsersRolesByUsrId() {
        return usrUsersRolesByUsrId;
    }

    public void setUsrUsersRolesByUsrId(Collection<UsrUsersRolesEntity> usrUsersRolesByUsrId) {
        this.usrUsersRolesByUsrId = usrUsersRolesByUsrId;
    }
}
