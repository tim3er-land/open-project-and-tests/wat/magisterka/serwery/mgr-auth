package pl.wat.mgr.witowski.ms.mgsauth.config;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;
import pl.wat.mgr.witowski.ms.mgsauth.security.JwtSecretGenerator;

import javax.annotation.PostConstruct;

@Log4j2
@Configuration
@EntityScan("pl.wat.mgr.witowski.ms.mgsauth.database.entity")
@ComponentScan({"pl.wat.mgr.witowski.ms.mgsauth"})
@EnableJpaRepositories("pl.wat.mgr.witowski.ms.mgsauth.database.repository")
public class AppConfig {
    @PostConstruct
    public void init() {
        JwtSecretGenerator.getPublicKey();
    }

    @Bean
    public RestTemplate restTemplate() {
        log.debug("Creating bean restTemplate");
        return new RestTemplate();
    }
}
