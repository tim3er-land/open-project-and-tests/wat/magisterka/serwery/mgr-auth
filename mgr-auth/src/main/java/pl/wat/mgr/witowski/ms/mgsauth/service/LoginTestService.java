package pl.wat.mgr.witowski.ms.mgsauth.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import pl.wat.mgr.witowski.ms.mgsauth.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.ms.mgsauth.security.jwt.JwtUtils;

@Log4j2
@Service
public class LoginTestService {
    public void testLogin() {
        if (!JwtUtils.getRole().contains("ADMIN")) {
            throw new ApiExceptions(HttpStatus.UNAUTHORIZED, "auth-401-002-001", "auth-002-001-401");
        }
    }
}
