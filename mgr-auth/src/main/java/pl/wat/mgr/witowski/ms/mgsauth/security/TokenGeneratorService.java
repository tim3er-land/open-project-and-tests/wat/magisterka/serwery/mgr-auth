package pl.wat.mgr.witowski.ms.mgsauth.security;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.wat.mgr.witowski.ms.mgsauth.database.entity.UsrUsersEntity;
import pl.wat.mgr.witowski.ms.mgsauth.dto.TokenDto;
import pl.wat.mgr.witowski.ms.mgsauth.security.jwt.JwtUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Log4j2
@Service
public class TokenGeneratorService {


    private final Long tokenExpire;

    public TokenGeneratorService(@Value("${pl.wat.mgr.witowski.ms.mgrauth.autorization.token.expire}") Long tokenExpire) {
        this.tokenExpire = tokenExpire;
    }

    public TokenDto generateJwtToken(UsrUsersEntity usrUsers, String tokenUid, List<String> roles) {

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime future = now.plusMinutes(tokenExpire);

        JwtBuilder builder = Jwts.builder()
                .claim(JwtUtils.KEY_UUID, usrUsers.getUsrUid())
                .claim(JwtUtils.KEY_LOGIN, usrUsers.getUsrLogin())
                .claim(JwtUtils.KEY_TUID, tokenUid)
                .claim(JwtUtils.KEY_ROLE, roles)
                .setIssuedAt(Date.from(now.atZone(ZoneId.systemDefault()).toInstant()))
                .setExpiration(Date.from(future.atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.RS512, JwtSecretGenerator.getPrivateKey());

        return TokenDto.builder()
                .tokenCreate(now.toString())
                .tokenExpire(future.toString())
                .token(builder.compact()).build();
    }
}
