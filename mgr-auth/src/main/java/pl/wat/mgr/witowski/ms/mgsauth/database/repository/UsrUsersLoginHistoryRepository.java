package pl.wat.mgr.witowski.ms.mgsauth.database.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.wat.mgr.witowski.ms.mgsauth.database.entity.UsrUsersLogingHistoryEntity;

@Repository
public interface UsrUsersLoginHistoryRepository extends JpaRepository<UsrUsersLogingHistoryEntity, String> {

}
