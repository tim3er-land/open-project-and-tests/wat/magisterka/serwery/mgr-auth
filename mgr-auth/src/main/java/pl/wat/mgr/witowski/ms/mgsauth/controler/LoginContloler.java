package pl.wat.mgr.witowski.ms.mgsauth.controler;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.wat.mgr.witowski.ms.mgsauth.dto.LoginDto;
import pl.wat.mgr.witowski.ms.mgsauth.dto.TokenDto;
import pl.wat.mgr.witowski.ms.mgsauth.service.LoginService;
import pl.wat.mgr.witowski.ms.mgsauth.service.LoginTestService;

import javax.servlet.http.HttpServletRequest;


@Log4j2
@CrossOrigin
@RestController
public class LoginContloler {

    private final LoginService loginService;
    private final LoginTestService loginTestService;

    @Autowired
    public LoginContloler(LoginService loginService, LoginTestService loginTestService) {
        this.loginService = loginService;
        this.loginTestService = loginTestService;
    }

    @PostMapping("/login")//001
    public ResponseEntity<TokenDto> login(@RequestBody(required = true) LoginDto loginDto, @RequestParam(value = "type", defaultValue = "USER") String userType, HttpServletRequest request) {
        log.info("Start method login with params[body: {} , type: {}]", loginDto.toString(), userType);
        TokenDto login = loginService.login(loginDto, request, userType);
        log.info("End method login with params[body: {} , type: {}]", loginDto.toString(), userType);
        return new ResponseEntity<>(login, HttpStatus.OK);
    }

    @PostMapping("/test")//002
    @PreAuthorize("hasAnyRole('INNER_USER')")
    public ResponseEntity<Void> test() {
        log.info("Start method test");
        loginTestService.testLogin();
        log.info("End method test");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
