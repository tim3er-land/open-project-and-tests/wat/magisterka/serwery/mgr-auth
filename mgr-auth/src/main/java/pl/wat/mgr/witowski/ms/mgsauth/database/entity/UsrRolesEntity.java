package pl.wat.mgr.witowski.ms.mgsauth.database.entity;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "usr_roles")
public class UsrRolesEntity {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "rl_id")
    private long rlId;
    @Basic
    @Column(name = "rl_code")
    private String rlCode;
    @Basic
    @Column(name = "rl_name")
    private String rlName;
    @Basic
    @Column(name = "rl_description")
    private String rlDescription;
    @Basic
    @Column(name = "rl_act")
    private short rlAct;
    @OneToMany(mappedBy = "usrRolesByRlRlId")
    private Collection<UsrUsersRolesEntity> usrUsersRolesByRlId;

    public long getRlId() {
        return rlId;
    }

    public void setRlId(long rlId) {
        this.rlId = rlId;
    }

    public String getRlCode() {
        return rlCode;
    }

    public void setRlCode(String rlCode) {
        this.rlCode = rlCode;
    }

    public String getRlName() {
        return rlName;
    }

    public void setRlName(String rlName) {
        this.rlName = rlName;
    }

    public String getRlDescription() {
        return rlDescription;
    }

    public void setRlDescription(String rlDescription) {
        this.rlDescription = rlDescription;
    }

    public short getRlAct() {
        return rlAct;
    }

    public void setRlAct(short rlAct) {
        this.rlAct = rlAct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsrRolesEntity that = (UsrRolesEntity) o;

        if (rlId != that.rlId) return false;
        if (rlAct != that.rlAct) return false;
        if (rlCode != null ? !rlCode.equals(that.rlCode) : that.rlCode != null) return false;
        if (rlName != null ? !rlName.equals(that.rlName) : that.rlName != null) return false;
        if (rlDescription != null ? !rlDescription.equals(that.rlDescription) : that.rlDescription != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (rlId ^ (rlId >>> 32));
        result = 31 * result + (rlCode != null ? rlCode.hashCode() : 0);
        result = 31 * result + (rlName != null ? rlName.hashCode() : 0);
        result = 31 * result + (rlDescription != null ? rlDescription.hashCode() : 0);
        result = 31 * result + (int) rlAct;
        return result;
    }

    public Collection<UsrUsersRolesEntity> getUsrUsersRolesByRlId() {
        return usrUsersRolesByRlId;
    }

    public void setUsrUsersRolesByRlId(Collection<UsrUsersRolesEntity> usrUsersRolesByRlId) {
        this.usrUsersRolesByRlId = usrUsersRolesByRlId;
    }

    public UsrRolesEntity(String rlCode) {
        this.rlCode = rlCode;
    }

    public UsrRolesEntity() {
    }
}
