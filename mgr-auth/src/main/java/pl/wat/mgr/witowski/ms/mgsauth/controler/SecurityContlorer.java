package pl.wat.mgr.witowski.ms.mgsauth.controler;


import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.wat.mgr.witowski.ms.mgsauth.dto.FullKeyDto;
import pl.wat.mgr.witowski.ms.mgsauth.security.secret.SecretDto;
import pl.wat.mgr.witowski.ms.mgsauth.service.SecurityService;

@Log4j2
@CrossOrigin
@RestController
public class SecurityContlorer {

    private final SecurityService securityService;

    @Autowired
    public SecurityContlorer(SecurityService securityService) {
        this.securityService = securityService;
    }

    @GetMapping("/security/key")//004
    @PreAuthorize("hasAnyRole('INNER_USER')")
    public ResponseEntity<SecretDto> generateNewKey() {
        log.info("Start generate new key for jwt");
        SecretDto secretDto = securityService.generateNewKeyV2();
        log.info("End generate new key for jwt");
        return new ResponseEntity<>(secretDto, HttpStatus.CREATED);
    }

    @GetMapping("/security/pair")//005
    @PreAuthorize("hasAnyRole('INNER_USER')")
    public ResponseEntity<FullKeyDto> getPairKey() {
        log.info("Start method getPairKey");
        FullKeyDto fullKeyDto = securityService.getPairKey();
        log.info("End method getPairKey");
        return new ResponseEntity<>(fullKeyDto, HttpStatus.OK);

    }
}
