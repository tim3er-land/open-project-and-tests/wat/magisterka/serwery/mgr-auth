package pl.wat.mgr.witowski.ms.mgsauth.security;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import pl.wat.mgr.witowski.ms.mgsauth.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.ms.mgsauth.security.jwt.JwtUtils;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.util.Base64;

@Log4j2
public class JwtSecretGenerator {
    private static KeyPair keyPair;

    public static String getPublicKey() {
        if (keyPair == null) {
            try {
                log.info("start generate new pair key");
                KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
                kpg.initialize(2048);
                JwtSecretGenerator.keyPair = kpg.generateKeyPair();
                log.info("generate new pair key");
            } catch (Exception ex) {
                log.error("Error occured while generate new pair key ");
                throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "auth-400-004-001", "auth-400-004-001");
            }
        } else {
            log.debug("keyPair already generated");
        }
        JwtUtils.setSecretKey(keyPair.getPublic());
        return Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded());
    }

    public static PrivateKey getPrivateKey() {
        if (keyPair == null) {
            try {
                log.info("start generate new pair key");
                KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
                kpg.initialize(2048);
                JwtSecretGenerator.keyPair = kpg.generateKeyPair();
                log.info("generate new pair key");
            } catch (Exception ex) {
                log.error("Error occured while generate new pair key ");
                throw new ApiExceptions(HttpStatus.INTERNAL_SERVER_ERROR, "auth-400-004-002", "auth-400-004-002");
            }
        } else {
            log.debug("keyPair already generated");
        }
        return keyPair.getPrivate();
    }
}
