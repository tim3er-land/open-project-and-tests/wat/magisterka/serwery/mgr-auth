package pl.wat.mgr.witowski.ms.mgsauth.service;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.wat.mgr.witowski.ms.mgsauth.dto.FullKeyDto;
import pl.wat.mgr.witowski.ms.mgsauth.security.secret.SecretDto;

public class SecurityServiceTest {
    private SecurityService securityService;

    @BeforeEach
    void setUp() {
        securityService = new SecurityService();
    }

    @Test
    void generateNewKeyTest() {
        SecretDto secretDto = securityService.generateNewKeyV2();
        Assert.assertNotNull(secretDto);
    }

    @Test
    void getPairKeyTest() {
        FullKeyDto pairKey = securityService.getPairKey();
        Assert.assertNotNull(pairKey);
        Assert.assertNotNull(pairKey.getPriKey());
        Assert.assertNotNull(pairKey.getPubKey());
        Assert.assertNotEquals(pairKey.getPubKey(), pairKey.getPriKey());
    }
}
