package pl.wat.mgr.witowski.ms.mgsauth.service;

import com.github.javafaker.Faker;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import pl.wat.mgr.witowski.ms.mgsauth.commons.exceptions.ApiExceptions;
import pl.wat.mgr.witowski.ms.mgsauth.database.entity.UsrRolesEntity;
import pl.wat.mgr.witowski.ms.mgsauth.database.entity.UsrUsersEntity;
import pl.wat.mgr.witowski.ms.mgsauth.database.repository.UsrRoleRepository;
import pl.wat.mgr.witowski.ms.mgsauth.database.repository.UsrUsersLoginHistoryRepository;
import pl.wat.mgr.witowski.ms.mgsauth.database.repository.UsrUsersRepository;
import pl.wat.mgr.witowski.ms.mgsauth.dto.LoginDto;
import pl.wat.mgr.witowski.ms.mgsauth.security.TokenGeneratorService;
import pl.wat.mgr.witowski.ms.mgsauth.security.jwt.SecurityUtils;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class LoginServiceTests {

    private LoginService loginService;
    private UsrUsersRepository usrUsersRepository;
    private TokenGeneratorService tokenGeneratorService;
    private UsrUsersLoginHistoryRepository usrUsersLoginHistoryRepository;
    private UsrRoleRepository usrRoleRepository;
    private Integer loginErrorCounter;
    private Faker faker;

    @BeforeEach
    void setUp() {
        faker = new Faker();
        usrUsersRepository = Mockito.mock(UsrUsersRepository.class);
        tokenGeneratorService = Mockito.mock(TokenGeneratorService.class);
        usrUsersLoginHistoryRepository = Mockito.mock(UsrUsersLoginHistoryRepository.class);
        usrRoleRepository = Mockito.mock(UsrRoleRepository.class);
        loginService = new LoginService(usrUsersRepository, tokenGeneratorService, usrUsersLoginHistoryRepository, usrRoleRepository, 10);
    }

    @Test
    void loginNotFountLoginTest() {
        try {
            loginService.login(new LoginDto(), preapareRequest(), "USER");
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getStatus(), HttpStatus.BAD_REQUEST);
            Assert.assertEquals(ap.getCode(), "auth-400-001-001");
            Assert.assertEquals(ap.getDescription(), "auth-400-001-001");
        }
    }

    @Test
    void userNotFoundTest() {
        try {
            when(usrUsersRepository.findUserForLogin(any(), any())).thenReturn(null);
            loginService.login(prepateLoginDto(), preapareRequest(), "USER");
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getStatus(), HttpStatus.UNAUTHORIZED);
            Assert.assertEquals(ap.getCode(), "auth-401-001-002");
            Assert.assertEquals(ap.getDescription(), "auth-401-001-002");
        }
    }

    private HttpServletRequest preapareRequest() {
        return new HttpServletRequest() {
            @Override
            public Object getAttribute(String s) {
                return null;
            }

            @Override
            public Enumeration<String> getAttributeNames() {
                return null;
            }

            @Override
            public String getCharacterEncoding() {
                return null;
            }

            @Override
            public void setCharacterEncoding(String s) throws UnsupportedEncodingException {

            }

            @Override
            public int getContentLength() {
                return 0;
            }

            @Override
            public long getContentLengthLong() {
                return 0;
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public ServletInputStream getInputStream() throws IOException {
                return null;
            }

            @Override
            public String getParameter(String s) {
                return null;
            }

            @Override
            public Enumeration<String> getParameterNames() {
                return null;
            }

            @Override
            public String[] getParameterValues(String s) {
                return new String[0];
            }

            @Override
            public Map<String, String[]> getParameterMap() {
                return null;
            }

            @Override
            public String getProtocol() {
                return null;
            }

            @Override
            public String getScheme() {
                return null;
            }

            @Override
            public String getServerName() {
                return null;
            }

            @Override
            public int getServerPort() {
                return 0;
            }

            @Override
            public BufferedReader getReader() throws IOException {
                return null;
            }

            @Override
            public String getRemoteAddr() {
                return "null";
            }

            @Override
            public String getRemoteHost() {
                return null;
            }

            @Override
            public void setAttribute(String s, Object o) {

            }

            @Override
            public void removeAttribute(String s) {

            }

            @Override
            public Locale getLocale() {
                return null;
            }

            @Override
            public Enumeration<Locale> getLocales() {
                return null;
            }

            @Override
            public boolean isSecure() {
                return false;
            }

            @Override
            public RequestDispatcher getRequestDispatcher(String s) {
                return null;
            }

            @Override
            public String getRealPath(String s) {
                return null;
            }

            @Override
            public int getRemotePort() {
                return 0;
            }

            @Override
            public String getLocalName() {
                return null;
            }

            @Override
            public String getLocalAddr() {
                return null;
            }

            @Override
            public int getLocalPort() {
                return 0;
            }

            @Override
            public ServletContext getServletContext() {
                return null;
            }

            @Override
            public AsyncContext startAsync() throws IllegalStateException {
                return null;
            }

            @Override
            public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) throws IllegalStateException {
                return null;
            }

            @Override
            public boolean isAsyncStarted() {
                return false;
            }

            @Override
            public boolean isAsyncSupported() {
                return false;
            }

            @Override
            public AsyncContext getAsyncContext() {
                return null;
            }

            @Override
            public DispatcherType getDispatcherType() {
                return null;
            }

            @Override
            public String getAuthType() {
                return null;
            }

            @Override
            public Cookie[] getCookies() {
                return new Cookie[0];
            }

            @Override
            public long getDateHeader(String s) {
                return 0;
            }

            @Override
            public String getHeader(String s) {
                return null;
            }

            @Override
            public Enumeration<String> getHeaders(String s) {
                return null;
            }

            @Override
            public Enumeration<String> getHeaderNames() {
                return null;
            }

            @Override
            public int getIntHeader(String s) {
                return 0;
            }

            @Override
            public HttpServletMapping getHttpServletMapping() {
                return HttpServletRequest.super.getHttpServletMapping();
            }

            @Override
            public String getMethod() {
                return null;
            }

            @Override
            public String getPathInfo() {
                return null;
            }

            @Override
            public String getPathTranslated() {
                return null;
            }

            @Override
            public PushBuilder newPushBuilder() {
                return HttpServletRequest.super.newPushBuilder();
            }

            @Override
            public String getContextPath() {
                return null;
            }

            @Override
            public String getQueryString() {
                return null;
            }

            @Override
            public String getRemoteUser() {
                return null;
            }

            @Override
            public boolean isUserInRole(String s) {
                return false;
            }

            @Override
            public Principal getUserPrincipal() {
                return null;
            }

            @Override
            public String getRequestedSessionId() {
                return null;
            }

            @Override
            public String getRequestURI() {
                return null;
            }

            @Override
            public StringBuffer getRequestURL() {
                return null;
            }

            @Override
            public String getServletPath() {
                return null;
            }

            @Override
            public HttpSession getSession(boolean b) {
                return null;
            }

            @Override
            public HttpSession getSession() {
                return null;
            }

            @Override
            public String changeSessionId() {
                return null;
            }

            @Override
            public boolean isRequestedSessionIdValid() {
                return false;
            }

            @Override
            public boolean isRequestedSessionIdFromCookie() {
                return false;
            }

            @Override
            public boolean isRequestedSessionIdFromURL() {
                return false;
            }

            @Override
            public boolean isRequestedSessionIdFromUrl() {
                return false;
            }

            @Override
            public boolean authenticate(HttpServletResponse httpServletResponse) throws IOException, ServletException {
                return false;
            }

            @Override
            public void login(String s, String s1) throws ServletException {

            }

            @Override
            public void logout() throws ServletException {

            }

            @Override
            public Collection<Part> getParts() throws IOException, ServletException {
                return null;
            }

            @Override
            public Part getPart(String s) throws IOException, ServletException {
                return null;
            }

            @Override
            public <T extends HttpUpgradeHandler> T upgrade(Class<T> aClass) throws IOException, ServletException {
                return null;
            }

            @Override
            public Map<String, String> getTrailerFields() {
                return HttpServletRequest.super.getTrailerFields();
            }

            @Override
            public boolean isTrailerFieldsReady() {
                return HttpServletRequest.super.isTrailerFieldsReady();
            }
        };
    }

    @Test
    void userPasswordWrongTest() {
        try {
            LoginDto loginDto = prepateLoginDto();
            UsrUsersEntity usrUsersEntity = prepareUserEntity(loginDto);
            usrUsersEntity.setUsrPass(SecurityUtils.hashString(faker.crypto().md5()));
            usrUsersEntity.setUsrErrorLogin(0);
            when(usrUsersRepository.findUserForLogin(any(), any())).thenReturn(usrUsersEntity);
            loginService.login(loginDto, preapareRequest(), "USER");
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getStatus(), HttpStatus.UNAUTHORIZED);
            Assert.assertEquals(ap.getCode(), "auth-401-001-003");
            Assert.assertEquals(ap.getDescription(), "auth-401-001-003");
        }
    }

    @Test
    void userPasswordWrongAndBlockUserTest() {
        try {
            LoginDto loginDto = prepateLoginDto();
            UsrUsersEntity usrUsersEntity = prepareUserEntity(loginDto);
            usrUsersEntity.setUsrPass(SecurityUtils.hashString(faker.crypto().md5()));
            usrUsersEntity.setUsrErrorLogin(11);
            when(usrUsersRepository.findUserForLogin(any(), any())).thenReturn(usrUsersEntity);
            loginService.login(loginDto, preapareRequest(), "USER");
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getStatus(), HttpStatus.UNAUTHORIZED);
            Assert.assertEquals(ap.getCode(), "auth-401-001-004");
            Assert.assertEquals(ap.getDescription(), "auth-401-001-004");
        }
    }

    @Test
    void userCorrectButUserHasNoRoleTest() {
        try {
            LoginDto loginDto = prepateLoginDto();
            UsrUsersEntity usrUsersEntity = prepareUserEntity(loginDto);
            when(usrUsersRepository.findUserForLogin(any(), any())).thenReturn(usrUsersEntity);
            when(usrRoleRepository.getUserRoles(usrUsersEntity)).thenReturn(null);
            loginService.login(loginDto, preapareRequest(), "USER");
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getStatus(), HttpStatus.UNAUTHORIZED);
            Assert.assertEquals(ap.getCode(), "auth-401-001-005");
            Assert.assertEquals(ap.getDescription(), "auth-401-001-005");
        }
    }

    @Test
    void loginCorrectTest() {
        try {
            LoginDto loginDto = prepateLoginDto();
            UsrUsersEntity usrUsersEntity = prepareUserEntity(loginDto);
            when(usrUsersRepository.findUserForLogin(any(), any())).thenReturn(usrUsersEntity);
            when(usrRoleRepository.getUserRoles(usrUsersEntity)).thenReturn(Arrays.asList(new UsrRolesEntity("TEST"), new UsrRolesEntity("TEST1")));

            when(tokenGeneratorService.generateJwtToken(any(), anyString(), anyList()))
                    .thenReturn(new TokenGeneratorService(10000l).generateJwtToken(usrUsersEntity, UUID.randomUUID().toString(), Arrays.asList("TEST", "TEST1")));
            loginService.login(loginDto, preapareRequest(), "USER");
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getStatus(), HttpStatus.UNAUTHORIZED);
            Assert.assertEquals(ap.getCode(), "auth-401-001-004");
            Assert.assertEquals(ap.getDescription(), "auth-401-001-004");
        }
    }

    @Test
    void errorWhileGenerateTokenTest() {
        try {
            LoginDto loginDto = prepateLoginDto();
            UsrUsersEntity usrUsersEntity = prepareUserEntity(loginDto);
            when(usrUsersRepository.findUserForLogin(any(), any())).thenReturn(usrUsersEntity);
            when(usrRoleRepository.getUserRoles(usrUsersEntity)).thenReturn(Arrays.asList(new UsrRolesEntity("TEST"), new UsrRolesEntity("TEST1")));
            when(tokenGeneratorService.generateJwtToken(any(), any(), any())).thenThrow(new NullPointerException());
            loginService.login(loginDto, preapareRequest(), "USER");
        } catch (ApiExceptions ap) {
            Assert.assertNotNull(ap);
            Assert.assertEquals(ap.getStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
            Assert.assertEquals(ap.getCode(), "auth-500-001-006");
            Assert.assertEquals(ap.getDescription(), "auth-500-001-006");
        }
    }

    private UsrUsersEntity prepareUserEntity(LoginDto loginDto) {
        UsrUsersEntity usrUsersEntity = new UsrUsersEntity();
        usrUsersEntity.setUsrAct((short) 1);
        usrUsersEntity.setUsrLogin(loginDto.getLogin());
        usrUsersEntity.setUsrPass(SecurityUtils.hashString(loginDto.getPassword()));
        usrUsersEntity.setUsrErrorLogin(0);
        return usrUsersEntity;
    }

    private LoginDto prepateLoginDto() {
        return LoginDto.builder()
                .login(faker.name().username())
                .password(faker.crypto().md5())
                .build();
    }
}
